#pragma once

class CookingStep;

/////////////////////////////////////////
// A recipe for making a dish.
/////////////////////////////////////////
class Recipe
{
private:
    // Array of cooking steps
    CookingStep** m_ppSteps; 

    // Number of steps
    size_t m_stepCount;

public:
    Recipe(int stepCount);
    ~Recipe();

    void Set(int index, CookingStep* step);

    // Cooks using the recipe.
    // Do not change! Do not override!
    void Cook();
};