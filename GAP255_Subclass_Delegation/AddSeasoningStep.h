#pragma once

#include "CookingStep.h"

class AddSeasoningStep : public CookingStep
{
public:
	virtual void Execute() override;
};

