#include "BoilWaterStep.h"

#include <iostream>

BoilWaterStep::BoilWaterStep(float mlWater)
	: m_mlWater(mlWater)
{

}

void BoilWaterStep::Execute()
{
	std::cout << "["  << __FUNCTION__ << "] Boil " << m_mlWater << " ml of watter" << std::endl;
}
