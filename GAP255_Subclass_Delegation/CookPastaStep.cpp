#include "CookPastaStep.h"

#include <iostream>

void CookPastaStep::Execute()
{
	std::cout << "[" << __FUNCTION__ << "] Time to cook some pasta!!" << std::endl;
}