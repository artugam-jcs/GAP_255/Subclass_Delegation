#include "Recipe.h"

#include <iostream>

#include "BoilWaterStep.h"
#include "AddSeasoningStep.h"
#include "PrepareStep.h"
#include "EnjoyMealStep.h"
#include "CookPastaStep.h"
#include "CookDumplingStep.h"

int main()
{
	// Reused steps
	auto boilWater = BoilWaterStep(5);
	auto addSeasoning = AddSeasoningStep();

	// This creates a recipe with 5 steps
	Recipe pasteRecipe{ 5 };
	{
		std::cout << ":: Start cooking pasta..." << std::endl;

		auto prepareSetp = PrepareStep("Prepare water and pasta noodles!");
		auto enojyMealStep = EnjoyMealStep("Pasta");
		auto cookPastaStep = CookPastaStep();

		pasteRecipe.Set(0, &prepareSetp);
		pasteRecipe.Set(1, &boilWater);
		pasteRecipe.Set(2, &cookPastaStep);
		pasteRecipe.Set(3, &addSeasoning);
		pasteRecipe.Set(4, &enojyMealStep);

		pasteRecipe.Cook();
	}

	Recipe dumplingRecipe{ 5 };
	{
		std::cout << ":: Start cooking dumpling..." << std::endl;

		auto prepareSetp = PrepareStep("Prepare wrappers and filling!");
		auto enojyMealStep = EnjoyMealStep("Dumpling");
		auto cookDumplingStep = CookDumplingStep();

		dumplingRecipe.Set(0, &prepareSetp);
		dumplingRecipe.Set(1, &boilWater);
		dumplingRecipe.Set(2, &cookDumplingStep);
		dumplingRecipe.Set(3, &addSeasoning);
		dumplingRecipe.Set(4, &enojyMealStep);

		dumplingRecipe.Cook();
	}

	return 0;
}