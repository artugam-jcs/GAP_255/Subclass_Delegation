#pragma once

#include "CookingStep.h"

class BoilWaterStep : public CookingStep
{
private:
	float m_mlWater;

public:
	BoilWaterStep(float mlWater);

	virtual void Execute() override;
};

