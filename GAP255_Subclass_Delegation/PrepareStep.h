#pragma once

#include <string>

#include "CookingStep.h"

class PrepareStep : public CookingStep
{
private:
	std::string m_description;

public:
	PrepareStep(std::string);

	virtual void Execute() override;
};

