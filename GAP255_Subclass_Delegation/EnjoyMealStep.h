#pragma once

#include <string>

#include "CookingStep.h"

class EnjoyMealStep : public CookingStep
{
private:
	std::string m_name;

public:
	EnjoyMealStep(std::string);

	virtual void Execute() override;
};

