#include "Recipe.h"
#include "CookingStep.h"

Recipe::Recipe(int stepCount)
    : m_stepCount(stepCount)
{
    m_ppSteps = new CookingStep*[m_stepCount];
}

Recipe::~Recipe()
{
    delete m_ppSteps;
}

void Recipe::Set(int index, CookingStep* step)
{
    m_ppSteps[index] = step;
}

/////////////////////////////////////////
// Cooks using the recipe.
// Do not change! Do not override!
/////////////////////////////////////////
void Recipe::Cook()
{
    for (size_t i = 0; i < m_stepCount; ++i)
    {
        m_ppSteps[i]->Execute();
    }
}
