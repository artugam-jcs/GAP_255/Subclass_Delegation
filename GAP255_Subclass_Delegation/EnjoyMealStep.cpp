#include "EnjoyMealStep.h"

#include <iostream>

EnjoyMealStep::EnjoyMealStep(std::string name)
	: m_name(name)
{

}

void EnjoyMealStep::Execute()
{
	std::cout << "[" << __FUNCTION__ << "] Enjoy eating " << m_name << "!!!~" << std::endl;
}
