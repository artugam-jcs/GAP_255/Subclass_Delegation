#include "PrepareStep.h"

#include <iostream>

PrepareStep::PrepareStep(std::string description)
	: m_description(description)
{

}

void PrepareStep::Execute()
{
	std::cout << "[" << __FUNCTION__ << "] " << m_description << std::endl;
}
